package com.lyq;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;


public class ServQuestionnaire extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static int recode = 1;

	static
	{
		Tool initTool = Tool.getInstance();
	}

	public ServQuestionnaire()
	{
		super();
    }

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		HttpSession session = request.getSession();

		String getRequestNum = (String) request.getParameter("requestNum");

		String[] splitResult = getRequestNum.split(",");

		int requestNum = Integer.parseInt(splitResult[1]);//当前请求的页数	
		
		if(session.getAttribute("sessionRequestNum")!=null && requestNum ==1)
		{
			requestNum = (int) session.getAttribute("sessionRequestNum");			
		}
		session.setAttribute("sessionRequestNum", requestNum);	
		
		
		String getAllAnswers = "";
		if(session.getAttribute("getAllAnswers")!= null)
		{
			getAllAnswers = (String) session.getAttribute("getAllAnswers");
		}
		session.setAttribute("getAllAnswers", getAllAnswers+splitResult[0]);//将考生所选择的答案存入session
		

		if(splitResult[0].length()>0)//当第一次请求页面和刷新页面时splitResult[0]="",即长度为0,此时不用解析答案
		{
			char[] charGetAnswers = splitResult[0].toCharArray();

			String correctAnswers = (String) session
					.getAttribute("correctAnswers");
			
			
			char[] charCorrectAnswers = correctAnswers.toCharArray();//存在问题

			int score=0;

			if(session.getAttribute("score") != null)
			{
				score = (int) session.getAttribute("score");
			}

			for (int i = 0; i < charGetAnswers.length; i++)
			{
				if(charGetAnswers[i]==charCorrectAnswers[i])
				{
					score++;
				}
			}

			// System.out.println("score: " + score);

			session.setAttribute("score", score);

		}

		// System.out.println("requestNum" + requestNum);

		if(requestNum <= 10)
		{
			getData(requestNum, request, response);
		}
		else
		{
			Calendar calendar = Calendar.getInstance();

			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH) + 1;
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			int hour = calendar.get(Calendar.HOUR);
			int minute = calendar.get(Calendar.MINUTE);
			int second = calendar.get(Calendar.SECOND);

			String[] resultStrings = new String[4];

			resultStrings[0] = (String) session.getAttribute("userName");
			resultStrings[1] = (String) session.getAttribute("studentId");
			resultStrings[2] = String.valueOf(session.getAttribute("score"));
			resultStrings[3] = year + "-" + month + "-" + day + " " + hour + ":"
					+ minute + ":" + second;

			int[] questionNo = (int[]) session.getAttribute("questionList");// 获取本次随机选择的题目编号
			String thisAllCorrectAnswers = (String) session
					.getAttribute("allCorrectAnswers");// 所有的正确答案
			String thisAllGetAnswers = (String) session
					.getAttribute("getAllAnswers"); // 所选择的所有答案

			// 控制台输出观测
			System.out.println("第" + (recode++) + "位学生");
			System.out.println("用户名: " + resultStrings[0]);
			System.out.println("学号: " + resultStrings[1]);
			System.out.println("得分: " + resultStrings[2]);
			System.out.println();

			try
			{
				Object obj = new Object();
				synchronized (obj)
				{
					Tool.writeExcle2(resultStrings);// 简单记录在result.xls文件中
					// 详细记录在Answers.xls文件中
					Tool.writeAnswewrsToExcle(resultStrings,
							thisAllCorrectAnswers, thisAllGetAnswers,
							questionNo);
				}

			}
			catch (BiffException | WriteException e)
			{
				e.printStackTrace();
			}
			session.removeAttribute("studentId");
			session.removeAttribute("questionList");
			session.removeAttribute("correctAnswers");
			session.removeAttribute("sessionRequestNum");
			session.removeAttribute("allCorrectAnswers");
			session.removeAttribute("getAllAnswers");

		}
	}

	public static void getData(int requestNum, HttpServletRequest request,
			HttpServletResponse response) throws IOException
	{

		response.setCharacterEncoding("utf-8");


		HttpSession session = request.getSession();

		// questionList为生成的题号随机数列表
		if(session.getAttribute("questionList") == null)
		{
			session.setAttribute("questionList", Tool.produceRandomNUm());
		}

		int[] index = new int[10];// 用于存放本次请求需要提取的十个题目的题号

		int begin = (requestNum - 1) * 10;

		int[] questionList = (int[]) session.getAttribute("questionList");

		for (int i = 0; i <= 9; i++)
		{
			index[i] = questionList[begin + i];
		}

		String[][] getStrings = Tool.returnQuestions(index, requestNum);

		String correctAnswers = "";

		for (int i = 0; i < getStrings.length; i++)
		{
			if(getStrings[i][6].trim().equals("对"))
			{
				correctAnswers += "A";
			}
			else if(getStrings[i][6].trim().equals("错"))
			{
				correctAnswers += "B";
			}
			else
			{
				correctAnswers += getStrings[i][6].trim();
			}
		}
		
		// System.out.println("correctAnswers: " + correctAnswers);
		
		session.removeAttribute("correctAnswers");
		session.setAttribute("correctAnswers", correctAnswers);
		
		String allCorrectAnswers = "";
		if(session.getAttribute("allCorrectAnswers")!=null)
		{
			allCorrectAnswers = allCorrectAnswers+(String)session.getAttribute("allCorrectAnswers");
			
		}
		session.setAttribute("allCorrectAnswers", allCorrectAnswers+correctAnswers);//将前面所有的正确答案和本次的正确答案一起存入sesseion
				
		PrintWriter out = response.getWriter();

		produceXML px = new produceXML();
		px.sendXML(out, getStrings);
		out.close();
		// System.out.println("test02");
	}

}
