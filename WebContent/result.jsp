<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>结果</title>
<link rel="stylesheet" href="style.css">
<script language="JavaScript" type="text/javascript" src="jquery-2.1.4.js"></script>
<script language="JavaScript" type="text/javascript" src="Ajax.js"></script>

</head>


<body>
	<div class="bodyStyle" id="question">	
		<%/* 
			if(session.getAttribute("userName")==null)
			{ */
		%>
			<%-- <jsp:forward page="login_nc.jsp"></jsp:forward> --%>
		<%
			/* }	 */	
		%>	
		<!-- ${userName}和${score}是从session中获取的值 -->
		<h1 style="font-family: '黑体'">考试结果：</h1>
		<div class="resultstyle">${userName}，您好，您本次考试得分为：【${score}】分</div>		
		
		<%
			session.removeAttribute("userName");
			session.removeAttribute("score"); 		
		%>
	</div>

</body>
</html>