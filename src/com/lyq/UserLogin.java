package com.lyq;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class UserLogin
 */
public class UserLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{

		System.out.println("in get");
		HttpSession session = request.getSession();

		// String data = request.getParameter("data");
		//
		// System.out.println(data);
		//
		// String[] getStrigs = data.split(",");
		//
		// String userName = getStrigs[0];
		//
		// String studentId = getStrigs[1];
		//
		// System.out.println("userName: "+userName);
		// System.out.println("studentId: " + studentId);

		String userName = request.getParameter("userName");
		String temp = new String(userName.getBytes("ISO-8859-1"), "utf-8");
		userName = URLDecoder.decode(temp, "utf-8");

		String studentId = request.getParameter("studentId");
		String temp2 = new String(studentId.getBytes("ISO-8859-1"), "utf-8");
		studentId = URLDecoder.decode(temp2, "utf-8");

		// 将用户名和密码放入session

		session.setAttribute("userName", userName);
		session.setAttribute("studentId", studentId);

		/* response.sendRedirect("showPage.jsp"); */

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		System.out.println("in post");

		HttpSession session = request.getSession();

		// String data = request.getParameter("data");
		//
		// System.out.println(data);
		//
		// String[] getStrigs = data.split(",");
		//
		// String userName = getStrigs[0];
		//
		// String studentId = getStrigs[1];
		//
		// System.out.println("userName: " + userName);
		// System.out.println("studentId: " + studentId);

		String userName = request.getParameter("userName");
		String temp = new String(userName.getBytes("ISO-8859-1"), "utf-8");
		userName = URLDecoder.decode(temp, "utf-8");

		String studentId = request.getParameter("studentID");
		String temp2 = new String(studentId.getBytes("ISO-8859-1"), "utf-8");
		studentId = URLDecoder.decode(temp2, "utf-8");

		System.out.println("userName: " + userName);
		System.out.println("studentId: " + studentId);

		// 将用户名和密码放入session

		session.setAttribute("userName", userName);
		session.setAttribute("studentId", studentId);

		/* response.sendRedirect("showPage.jsp"); */

	}

}
