package com.lyq;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Random;
import java.util.TreeSet;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class Tool
{
	// 题目总数
	private final static int ALLQUESTIONUM = 627;

	// 抽取题目数量
	private final static int GETQUESTNUM = 100;
	private static String[][] allQuestions;
	private static Tool tool;

	private Tool()
	{
		allQuestions = readExcel();
	}

	// 获取Tool类的实例
	public synchronized static Tool getInstance()
	{
		if(tool == null)
		{
			tool = new Tool();
		}
		return tool;
	}

	// 随机产生100个题目编号
	public static int[] produceRandomNUm()
	{
		int[] result = new int[GETQUESTNUM];
		Random random = new Random();

		TreeSet<Integer> treeSet = new TreeSet<Integer>();

		while (treeSet.size() < 100)
		{
			int getNum = random.nextInt(ALLQUESTIONUM);
			if(!treeSet.contains(getNum))
			{
				treeSet.add(getNum);
				// System.out.print(getNum + " ");
			}
		}

		int index = 0;

		for (Iterator<Integer> iterator = treeSet.iterator(); iterator
				.hasNext();)
		{
			result[index] = iterator.next();
			index++;
		}

		return result;
	}

	// 初始化该类时读取excel中所有的题目
	public static String[][] readExcel()
	{
		String[][] resultStrings = new String[ALLQUESTIONUM][7];

		jxl.Workbook readwb = null;

		try
		{
			// 直接从本地文件创建Workbook
			InputStream instream = new FileInputStream(
					"E:/2012年大学新生安全知识题库.xls");

			readwb = Workbook.getWorkbook(instream);

			// 获取第一张Sheet表
			Sheet readsheet = readwb.getSheet(0);

			// 获取Sheet表中所包含的总列数
			int rsColumns = 6;

			// 获取指定单元格的对象引用
			// row代表要取题目的行号
			for (int row = 0; row < ALLQUESTIONUM; row++)
			{
				// 设定题目编号为题目的实际编号，即为1-627
				resultStrings[row][0] = String.valueOf(row + 1);

				for (int col = 0; col < rsColumns; col++)
				{
					Cell cell = readsheet.getCell(col, row + 1);
					// 将题目中的回车换行变成html中的换行
					resultStrings[row][col + 1] = cell.getContents()
							.replaceAll("[\\t\\n\\r]", "<br>");
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			readwb.close();
		}

		return resultStrings;
	}

	// 根据servlet中传过来的题目序号和请求页数，返回所请求的题目内容以答案
	public static String[][] returnQuestions(int[] questionIndex,
			int requestNum)
	{
		String[][] questions = new String[questionIndex.length][7];
		
		int begin = (requestNum - 1) * 10+1;

		for (int index = 0; index < questionIndex.length; index++)
		{	
			questions[index] = allQuestions[questionIndex[index]];
			questions[index][0] = begin+index+"";
		}
		return questions;
	}


	// 将测试结果写入excel中
	public static void writeExcle2(String[] resultStrings) throws BiffException, IOException, RowsExceededException, WriteException			
	{
		Workbook wwb = Workbook.getWorkbook(new File("E:/Results.xls"));
					 
		WritableWorkbook book = Workbook
				.createWorkbook(new File("E:/Results.xls"),
				wwb);

		jxl.write.WritableSheet ws = (WritableSheet) book.getSheet(0);

		int rowNum = ws.getRows();
		for (int k = 0; k < 4; k++)
		{
			Label label = new Label(k, rowNum, resultStrings[k]);
			ws.addCell(label);
		}
		book.write();
		book.close();
		wwb.close();
	}
	
	
	// 将答案记录在excel中
	public static void writeAnswewrsToExcle(String[] resultStrings,String correctAnswers ,String getAnswers,int[] questionNo) throws BiffException, IOException, RowsExceededException, WriteException
	{
		//数据格式转换
		
		String[] answersC = new String[100];		
		char[] answersCArray = correctAnswers.toCharArray();		
		for (int q = 0; q < answersC.length; q++)
		{
			answersC[q] = answersCArray[q]+"";
		}
		
		String[] answersG = new String[100];
		char[] answersGArray = getAnswers.toCharArray();
		for (int p = 0; p < answersG.length; p++)
		{
			answersG[p] = answersGArray[p]+"";
		}
		
		Workbook wwb = Workbook.getWorkbook(new File("E:/Answers.xls"));
		
		WritableWorkbook book = Workbook.createWorkbook(new File("E:/Answers.xls"),	wwb);
		
		jxl.write.WritableSheet ws = (WritableSheet) book.getSheet(0);

		int rowNum = ws.getRows()+1;//获取当前已写最大行
		
		// 记录姓名、学号、分数、提交时间
		for (int k = 0; k < 4; k++)
		{
			Label label = new Label(k, rowNum, resultStrings[k]);
			ws.addCell(label);
		}
		
		// 记录所做题目的真实编号
		rowNum = ws.getRows();//获取当前已写最大行
		for(int m = 0;m<questionNo.length;m++)
		{
			Label label = new Label(m, rowNum, questionNo[m]+"");
			ws.addCell(label);			
		}
		
		// 记录所做题目的正确答案
		rowNum = ws.getRows();//获取当前已写最大行
		for(int j = 0;j<answersC.length;j++)
		{
			Label label = new Label(j, rowNum, answersC[j]);
			ws.addCell(label);			
		}
		
		// 记录考生所选的答案
		rowNum = ws.getRows();//获取当前已写最大行
		for(int i = 0;i<answersG.length;i++)
		{
			Label label = new Label(i, rowNum, answersG[i]);
			ws.addCell(label);			
		}
		
		book.write();
		book.close();
		wwb.close();
		
		
	}
	
	
	
	
	

}
