<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.net.URLDecoder" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新生安全知识考试</title>
<link rel="stylesheet" href="style.css">
<script language="JavaScript" type="text/javascript" src="jquery-2.1.4.js"></script>
<script language="JavaScript" type="text/javascript" src="Ajax.js"></script>

<script type="text/javascript">
	$(function(){		
		var pageIndex = 1;		
		var answers="";	 
		
		function dynamic(requestNum,answers){				
			
			$.ajax({ 
		    	type: "post", 
		    	url: "ServQuestionnaire", 
		    	data: "requestNum="+answers+","+requestNum,
		    	success: function(msgXML) /*  操作成功后的操作！msg是后台传过来的值  */
		    	{ 
		    		$(".question_style").remove();
		    		$("#nextPage").remove();
		    		
		    		var recoddeQuestionNo = 0;
		    		
		    		$(msgXML).find("question").each(function(v,vv) { 
		    			/* var questionNo = (requestNum-1)*10+v+1;//生成题目编号 */
		    			
		    			//获取题目内容及答案 
		    			var questionNo = $(this).find("questionNo").attr("questionNo");	
		    			
		    			/* alert("questionNo: "+questionNo); */
			    		var content = $(this).find("content").attr("content");			    		
			    		var answerA = $(this).find("answerA").attr("answerA");
			    		var answerB = $(this).find("answerB").attr("answerB");
			    		var answerC = $(this).find("answerC").attr("answerC");
			    		var answerD = $(this).find("answerD").attr("answerD");		    		
			    		
			    		//记录题目编号以备接下来矫正pageIndex
			    		recoddeQuestionNo = questionNo;	
			    		
			    		/* alert("pageIIII:"+pageIndex); */
			    		if(pageIndex<=10)
			    		{			    		
				    		//生成页面相应内容
				    		$("#question").append("<div class='question_style' id='extra"+questionNo+"'>"+questionNo+"."+content+"<br>"+
				 				 "<input type='radio' name='answer"+questionNo+"' value='A'><span class='questionchoice'>"+answerA+"</span><br>"+
							 	 "<input type='radio' name='answer"+questionNo+"' value='B'><span class='questionchoice'>"+answerB+"</span><br></div>");
							 if(answerC.length>0)
							 {
								 $("#extra"+questionNo).append("<input type='radio' name='answer"+questionNo+"' value='C'><span class='questionchoice'>"+answerC+"</span><br>");
							 }
				 			
							 if(answerD.length>0)
							 {
								 $("#extra"+questionNo).append("<input type='radio' name='answer"+questionNo+"' value='D'><span class='questionchoice'>"+answerD+"</span>");								 
							 }	
			    		}
		    		});	//success结束 
		    		
		    		//通过题目编号矫正pageIndex
		    		pageIndex = recoddeQuestionNo/10;
		    		
		    		//将页面显示定位到页面顶部 
		    		$('html,body').animate({scrollTop: '0px'}, 0);	    		  
		    		
		    		if(requestNum==10)//提交之前执行 
					{						
						$("#question").append("<br><br><button id='btuSubmit'>提交</button>");		    			
					}
		    		else if(requestNum<10)// 提交时页面 
		    		{
		    			$("#btnnex").remove();
		    			
						$('#question').append("<div id='btnnex'><br><br><button id='nextPage'>下一页</button></div>");
		    		}
		    		else if(requestNum==11)// 提交时跳转 
		    		{
		    			$("#btuSubmit").remove();
		    			location.href = "result.jsp";
		    		}		    		
		    	} 			
		    });			
		}
		dynamic(pageIndex,answers);
		
		//获取当前页面答案以及跳转到下一页 
		function getAnswerAndJump()
		{
			/* 取答案 */
			var questionNo = (pageIndex-1)*10+1; 
			answers ="";	
			var flag=1;			
			
			for(var i =0; i<10;i++)
			{				
				var list= $('input:radio[name="answer'+(questionNo+i)+'"]:checked').val(); 				
				if(list==null)
				{					
					alert("请完成本页第"+(i+1)+"题");
					flag=0;	
					break;
				}
				answers = answers+list;							
			}			
			
			/* 	跳转到下一页  */
			if(flag==1)
			{
				pageIndex = pageIndex+1;
				dynamic(pageIndex,answers);
			}			
		}//getAnswerAndJump()结束 
		
		//下一页按钮的点击事件
		$("div").delegate("#nextPage","click", function() 
		{		
			getAnswerAndJump();
		});
		
		//提交按钮的点击事件   
		$("div").delegate("#btuSubmit","click", function() 
		{	
			getAnswerAndJump();			
		}); //提交结束 
		
		
		//点击文本选择相应选项 
		$("div").delegate(".questionchoice","click", function() {			
			$(this).prev().trigger("click");			
		});
		
		
	});
	
	
</script>

</head>


<body >	
	<%
		/* if(request.getParameter("userName")==null||request.getParameter("studentId")==null)
		{ */
	%>
		<%-- <jsp:forward page="login_nc.jsp"></jsp:forward> --%>
	<%
		/* } */
	
		/* String userName = request.getParameter("userName");		 
		String temp = new String(userName.getBytes("ISO-8859-1"),"utf-8");  
		userName = URLDecoder.decode(temp, "utf-8");	
		
		String studentId = request.getParameter("studentId");
		String temp2 = new String(studentId.getBytes("ISO-8859-1"),"utf-8");  
		studentId = URLDecoder.decode(temp2, "utf-8");	
			
		//将用户名和密码放入session 
		session.setAttribute("userName", userName);
		session.setAttribute("studentId", studentId);	 */
		
		System.out.print("userName,get: "+session.getAttribute("userName"));
		
	%>
	<div class="headstyle">${userName}，欢迎你参加本次考试</div>
	
	<h1 style="font-family: 黑体; color: gray; margin-left: 80px;">新生安全知识考试</h1>

	<div class="bodyStyle" id="question"></div>
	
</body>
</html>